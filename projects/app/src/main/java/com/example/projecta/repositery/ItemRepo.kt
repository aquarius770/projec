package com.example.myapplication.repository

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.myapplication.data.ItemDAO
import com.example.myapplication.data.Items

class ItemRepo (private val itemDAO: ItemDAO){
    @Query("SELECT * FROM items WHERE  item_name = :title")
    fun getItemByName(title: String?): LiveData<Items>{

       return itemDAO.getItemByName(title)

    }

    fun insertItem(items: Items){
        itemDAO.insertItem(items)

    }
    fun updateItem(items: Items){
        itemDAO.updateItem(items)

    }
    fun deleteItem(items: Items){
        itemDAO.deleteItem(items)


    }
}