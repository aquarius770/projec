package com.example.myapplication.data

import androidx.lifecycle.LiveData
import androidx.room.*
@Dao
interface ItemDAO {

        @Query("SELECT * FROM items WHERE  item_name = :title")
        fun getItemByName(title: String?): LiveData<Items>

        @Insert(onConflict = OnConflictStrategy.REPLACE)
        fun insertItem(items: Items)

        @Update
        fun updateItem(Item: Items)

        @Delete
        fun deleteItem(Item: Items)
    }
