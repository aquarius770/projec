package et.edu.aait.roomdbexample.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.data.ItemDAO
import com.example.myapplication.data.Items
import com.example.myapplication.data.UserDao
import com.example.myapplication.data.Users

@Database(entities = arrayOf(Users::class,Items::class),version = 1)
abstract class MyDatabase: RoomDatabase() {
    // function that return Dao class
    abstract fun userDao(): UserDao
    abstract fun itemDAO(): ItemDAO

    companion object {

        @Volatile
        private var INSTANCE: MyDatabase? = null

        fun getDatabase(context: Context): MyDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java, "my_database"
                ).build()

                INSTANCE = instance
                return instance
            }

        }
    }
}

