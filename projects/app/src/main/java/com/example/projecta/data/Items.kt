package com.example.myapplication.data

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName="items")
data class Items(
    @NonNull
    @PrimaryKey(autoGenerate = true)

    @ColumnInfo(name = "id") val id:Int=0,
    @ColumnInfo(name="item_name") val title:String,
    @ColumnInfo(name="item_price") val price: Int,
    @ColumnInfo(name="item_description") val About: String,
    @ColumnInfo(name="Contact_info") val contact: Int): Serializable
