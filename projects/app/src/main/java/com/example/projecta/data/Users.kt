package com.example.myapplication.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName="user")
data class Users (@PrimaryKey @ColumnInfo(name="User_name") val name:String,
                    @ColumnInfo(name="User_password") val pass: String,
                    @ColumnInfo(name="User_contact") val contact: String):Serializable

